<?php
class  MyCalculator
{
    public $a,$b;
    public function __construct($a,$b)
    {
        $this->a=$a;
        $this->b=$b;
    }

    public function add()
    {
        return $this->a+$this->b;
    }

    public function multiply()
    {
        return $this->a*$this->b;
    }
    public function subtract()
    {
        return $this->a-$this->b;
    }
    public function division()
    {
        return $this->a/$this->b;
    }
}
$mycalc = new MyCalculator( 12, 6);
echo" My Calculator : <br>";
echo "Addition= ". $mycalc-> add()."<br>";
echo "Multiplication= ".$mycalc-> multiply()."<br>";
echo "Subtraction= ".$mycalc-> subtract()."<br>";
echo "Division= ".$mycalc-> division()."<br>";
<?php


 class factorial_of_a_number

 {

     protected $_number;

 public function __construct($number)

 {

         if (!is_int($number))

         {

             throw new InvalidArgumentException('Not a number or missing argument');

 }

 $this->_number = $number;

 }

 public function result (){
     $x=1;
     $i=$this->_number;
     for($i;$i>=1;$i--)
     {
         $x=$x*$i;
     }
     return $x;

 }

}

$newfactorial = New factorial_of_a_number(5);

echo "New factorial of a number(5)=".$newfactorial->result();